<?php
namespace Contract;

interface DataProviderInterface {

    public function get(RequestDto $request): ResponseDto;
}