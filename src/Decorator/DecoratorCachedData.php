<?php
namespace Decorator;

use Contract\DataProviderInterface;
use Psr\Cache\CacheItemPoolInterface;
use DateTimeImmutable;

class DecoratorCachedData extends DecoratorRawData {

    private $cache;

    public function __construct(DataProviderInterface $provider, CacheItemPoolInterface $cache)
    {
        parent::__construct($provider);
        $this->cache = $cache;
    }

    public function get(RequestDto $request): ResponseDto
    {
        $cacheKey = $this->getCacheKey($input);
        $cacheItem = $this->cache->getItem($cacheKey);

        if ($cacheItem->isHit()) {
            return $cacheItem->get();
        }

        $result = $this->provider->get($request);

        $cacheItem
                ->set($result)
                ->expiresAt(
                    (new DateTimeImmutable())->modify('+1 day')
                );

        return $result;
    }

    private function getCacheKey(RequestDto $input)
    {
        return json_encode($input);
    }
}