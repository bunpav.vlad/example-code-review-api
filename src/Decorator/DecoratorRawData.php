
<?php
namespace Decorator;

use Contract\DataProviderInterface;

class DecoratorRawData implements DataProvider
{
    protected $provider;

    public function __construct(DataProviderInterface $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param RequestDto $request
     *
     * @return ResponseDto
     */
    public function get(RequestDto $request): ResponseDto
    {
        return $this->provider->get($request);
    }
}