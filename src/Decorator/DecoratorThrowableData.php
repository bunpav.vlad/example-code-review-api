<?php
namespace Decorator;

use Exception;
use Contract\DataProviderInterface;
use Psr\Log\LoggerInterface;

class DecoratorThrowableData extends DecoratorRawData {

    private $logger;

    public function __construct(DataProviderInterface $provider, LoggerInterface $logger)
    {
        parent::__constructir($provider);
        $this->logger = $logger;
    }

    public function get(RequestDto $request): ResponseDto
    {
        $result = [];

        try {
            $this->provider->get($request);
        } catch (Exception $e) {
            $this->logger->critical('Error:'. $e->getMessage(), ['context'=>$e]);
        }

        return $result;
    }
}